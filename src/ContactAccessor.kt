package cordova.plugin

import android.provider.ContactsContract
import org.apache.cordova.CordovaInterface

class ContactAccessor(context: CordovaInterface) {

    var mApp: CordovaInterface = context

    fun getContacts(): MutableList<String> {
        val list: MutableList<String> = ArrayList()
        var builder = StringBuilder()

        val projection = arrayOf(
            ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
            ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
            ContactsContract.CommonDataKinds.Phone.NUMBER
        )
        val cursor =
            mApp.getActivity().getContentResolver().query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                projection,
                ContactsContract.Contacts.HAS_PHONE_NUMBER + ">0 AND LENGTH(" + ContactsContract.CommonDataKinds.Phone.NUMBER + ")>0",
                null,
                "display_name ASC"
            )

        if (cursor != null && cursor.count > 0) {
            while (cursor.moveToNext()) {
                builder = StringBuilder()
                val id =
                    cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID))
                val name =
                    cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME))
                val mobileNumber =
                    cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))

                builder.append("{\"id\": \"").append(id).append("\",")
                builder.append("\"name\": \"").append(name).append("\",")
                builder.append("\"mobileNumber\": \"").append(mobileNumber).append("\"}")

                list.add(builder.toString())

            }
            cursor.close()
        }
        return list
    }
}
